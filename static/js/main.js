jQuery('#mainnav-list li ul').css({display: 'none', opacity: 1});

jQuery('#mainnav-list li.hasSubmenu > ul > li').each(function() {
	jQuery(this).css('height', jQuery(this).parent('ul').height()+'px');
});

jQuery('#mainnav-list li').each(function() {

	var jQuerysublist = jQuery(this).find('ul:first');

		jQuery(this).hover(function() {

			position = jQuery(this).position();

			if(jQuery(this).parents().attr('class') == 'mainnavitem-submenu')
				{
					jQuerysublist.stop().fadeIn(500);
				}
				else
				{
					jQuerysublist.stop().css({overflow: 'visible'}).fadeIn(500);
				}
		},
		function() {

            jQuerysublist.stop().css({height:'auto'}).fadeOut(500);
		});

});


// Mobile navigation menu
jQuery('#mobile-nav-icon').on( 'click', function() {
    jQuery('body,html').animate({scrollTop:0},100);
    jQuery('body').toggleClass('js_nav');
    jQuery('body').addClass('overflow_hidden');

    if(is_touch_device())
    {
        jQuery('body.js_nav').css('overflow', 'auto');
    }
});

jQuery('#close_mobile_menu').on( 'click', function() {
    jQuery('body').removeClass('js_nav');
    jQuery('body').removeClass('overflow_hidden');
});

jQuery('body').on('click', '.mobile-main-nav > li a', function(event) {
    var jQuerysublist = jQuery(this).parent('li').find('ul.sub-menu:first');
    var menuContainerClass = jQuery(this).parent('li').parent('#mobile-main-menu.mobile-main-nav').parent('div');

    event.preventDefault();
    if(jQuerysublist.length>0)
    {
        event.preventDefault();
    }

    var menuLevel = 'top_level';
    var parentMenu = '';
    var menuClickedId = jQuery(this).attr('id');

    //if(jQuery(this).parent('li').parent('ul').attr('id')=='mobile_main_menu')
    //{
        menuLevel = 'parent_level';
    //}
    //else
    //{
    //    parentMenu = jQuery(this).parent('li').attr('id');
    //}

    //if(jQuerysublist.length>0)
    {
        jQuery('#mobile_main_menu.mobile_main_nav').addClass('mainnav_out');
        jQuery('.mobile_menu_wrapper div #sub_menu').removeClass('subnav_in');
        jQuery('.mobile_menu_wrapper div #sub_menu').addClass('mainnav_out');

        setTimeout(function() {
            jQuery('#mobile_main_menu.mobile_main_nav').css('display', 'none');
            jQuery('.mobile_menu_wrapper div #sub_menu').remove();

            var subMenuHTML = '<li><a href="#" id="menu_back" class="'+menuLevel+'" data-parent="'+parentMenu+'">'+jQuery('#pp_back').val()+'</a></li>';
            subMenuHTML += jQuerysublist.html();

            menuContainerClass.append('<ul id="sub_menu" class="nav '+menuLevel+'"></ul>');
            menuContainerClass.find('#sub_menu').html(subMenuHTML);
            menuContainerClass.find('#sub_menu').addClass('subnav_in');
        }, 200);
    }
});

jQuery('body').on('click', '#menu_back.parent_level', function() {
	jQuery('.mobile_menu_wrapper div #sub_menu').removeClass('subnav_in');
	jQuery('.mobile_menu_wrapper div #sub_menu').addClass('subnav_out');
	jQuery('#mobile_main_menu.mobile_main_nav').removeClass('mainnav_out');

	setTimeout(function() {
	jQuery('.mobile_menu_wrapper div #sub_menu').remove();
	jQuery('#mobile_main_menu.mobile_main_nav').css('display', 'block');
	jQuery('#mobile_main_menu.mobile_main_nav').addClass('mainnav_in');
}, 200);

});

$("#newsbutton-all").click(function(){
    $("#news_all").show();
    this.className += " active";
    $("#news_astronomy").hide();
    $("#news_space").hide();
    $("#news_astrophotography").hide();
});

$("#newsbutton-astronomy").click(function(){
    $("#news_all").hide();
    $("#news_astronomy").show();
    this.className += " active";
    $("#news_space").hide();
    $("#news_astrophotography").hide();
});

$("#newsbutton-space").click(function(){
    $("#news_all").hide();
    $("#news_astronomy").hide();
    $("#news_space").show();
    this.className += " active";
    $("#news_astrophotography").hide();
});

$("#newsbutton-astrophotography").click(function(){
    $("#news_all").hide();
    $("#news_astronomy").hide();
    $("#news_space").hide();
    $("#news_astrophotography").show();
    this.className += " active";
});